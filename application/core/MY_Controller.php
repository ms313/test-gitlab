<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class MY_Controller extends  CI_Controller{


		function __construct()
		{
			parent::__construct();
		}


	//set the class variable.

	   var $template  = array();
	   var $data      = array();
	//Load layout
	   public function layout() {
	    // making temlate and send data to view.
	     	$this->template['header']   = $this->load->view('profile/header', $this->data, true);

            $this->template['middle'] = $this->load->view($this->middle, $this->data, true);

	     	$this->template['top'] = $this->load->view('profile/top', $this->data, true);
				
            $this->load->view('profile/index', $this->template);
	   }
	   public function layout_login() {
	     	//$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
	     	///$this->load->view('layout/login_view', $this->data);
	     	$this->load->view('login/login_site', $this->data);

	   }
	   public function login() {
	    // making temlate and send data to view.
	     	$this->load->view('login/login_site', $this->data);
			//$this->template['middle'] = $this->load->view($this->middle, $this->data, true);

	     	//$this->load->view('login/index', $this->template);

	   }
     public function first_page() {
	    // making temlate and send data to view.
	     	$this->load->view('first_page');
			//$this->template['middle'] = $this->load->view($this->middle, $this->data, true);

	     	//$this->load->view('login/index', $this->template);

	   }
		 public function rigster_page(){
			 	$this->load->view('register_page');
		 }
		 public function login_page(){
			 	$this->load->view('login_page');
		 }
         public function admin_login_page(){
			 	$this->load->view('admin/login_page');
		 }
         public function admin_layout() {
	        // making temlate and send data to view.
	     	$this->template['header']   = $this->load->view('admin/header', $this->data, true);

            $this->template['middle'] = $this->load->view($this->middle, $this->data, true);

	     	$this->template['top'] = $this->load->view('admin/top', $this->data, true);
				
            $this->load->view('admin/index', $this->template);
	     }
	}
?>
