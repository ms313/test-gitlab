<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
 			parent::__construct();
 	}
	public function index(){
		//$this->middle ='welcome_message';
		$this->login_page();
	}
	public function user(){
	 $this->load->model('Login_model');
	 $model_user = new  Login_model();
	 $username   =  $_POST['username'] ;
	 $password   =  $_POST['password'] ;
	 $data       = array(
        'username'  => $username,
        'password'  => $password
	 );
	 $result =  $model_user->check_username_password($data);
	 if($result){
			 $status['success'] = true;
	 }else{
		  $status['fail_username'] = true;
	 }
	 echo json_encode($status);
	}
}
