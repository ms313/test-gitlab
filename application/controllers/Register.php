<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
 			parent::__construct();
 	}
	public function index(){
		//$this->middle ='welcome_message';
		$this->rigster_page();
	}
	public function reg(){

	 $this->load->model('Register_model');
	 $model_user = new  Register_model();
	 $username   =  $_POST['username'] ;
	 $password   =  $_POST['password'] ;
	 $email      =  $_POST['email'] ;
	 $company    =  $_POST['company'] ;
	 $address    =  $_POST['address'] ;
	 $tell       =  $_POST['cell_number'] ;
	 $data       = array(
        'username'  => $username,
        'password'  => $password,
        'email'  	=> $email,
        'company'   => $company,
        'address'   => $address,
        'tell'      => $tell
	 );
	 $cnt =  $model_user->check_username($data);
	 if(!($cnt>0)){
		 $insert_id = $model_user->insert_user($data);
		 if($insert_id){
			 $status['success'] = true;
		 }else{
			 $status['fail_server'] = true;
		 }
	 }else{
		 $status['fail_username'] = true;
	 }
	 echo json_encode($status);
	}
}
