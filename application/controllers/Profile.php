<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	/*public function index()
	{
		$this->load->view('welcome_message');
	}*/

class Profile extends MY_Controller {
    function __construct(){
       parent::__construct();
             $this->check_isvalidated();
    }
    private function check_isvalidated(){
            if(! $this->session->userdata('validated')){
                redirect('login');
            }
      }
    private function username_in_session(){
            return $this->session->userdata('username');
    }
    public function index() {
            $this->load->model('Profile_model');
            $model_profile = new  Profile_model();
            $username = $this->username_in_session();
            $user_id  = $model_profile->get_user_id($username);
            $user_id  = $user_id['0']->id;
            $count_product = $model_profile->count_user_of_product($user_id);
            $info['count_product'] = $count_product;
            $this->data = $info;
      $this->middle = 'profile/home';
        $this->layout();
    }
    public function add_product() {
        $this->load->model('Profile_model');
        $model_profile = new  Profile_model();
        $result  =  $model_profile->list_category();
        $info['html'] = '';
        $count = 1;
        $padding =0;
        foreach ($result as $value) {
            if($value->parent_id=='0'){
                    $info['html'].= "<option value=".$value->id.">"." $value->title "."</option>";
                    $info['html'].=$this->sub_category($result,$value,$count,$padding);
            }
        }
        $this->data =  $info;
        $this->middle = 'profile/add_product';
        $this->layout();
    }
    public function sub_category($result,$gabli,$count,$padding){
            $sub1 = '';

            foreach ($result as $feli) {
                    if($feli->parent_id !=='0' && $feli->parent_id == $gabli->id){
                        $count = $feli->parent_id;
                        for($i=0;$i<$count;$i++){
                                $padding=$padding+5;
                        }
                        $sub1.='<option  value='.$feli->id.' style='."padding-left:".$padding."px;".'>';
                        for($i=0;$i<$count;$i++){
                                $sub1.='-';
                        }
                        $sub1.=$feli->title."</option>";
                    }else{
                        continue;
                    }
                    $padding =0;
                    $sub1.=$this->sub_category($result,$feli,$count,$padding);
            }
            return $sub1;
    }
    public function save_product(){
        $this->load->model('Profile_model');
        $model_profile = new  Profile_model();
        $date  = new jDateTime(true, true, 'Asia/Tehran');
        $insert_date = $date->date("Y-m-d H:i:s", false, false);
        $username = $this->username_in_session();
        $user_id  = $model_profile->get_user_id($username);
        $user_id  = $user_id['0']->id;
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'pdp|txt|zip';
        $config['max_filename'] = '520';
        $config['max_size'] = '1024'; //1 MB
        if((!empty($_POST['product_name'])) && (!empty($_POST['price'])) && ((!empty($_POST['status'])) || $_POST['status'] == '0') && ((!empty($_POST['granty_time'])) || $_POST['granty_time'] == '0') &&
        ((!empty($_POST['granty_name'])) || $_POST['granty_name'] == '0') && ((!empty($_POST['category_id'])) || $_POST['category_id'] == '0') && ((!empty($_POST['workable'])) || $_POST['workable'] == '0')
        && ((!empty($_POST['count'])) || $_POST['count'] =='0')){
            $product_name = $_POST['product_name'];
            $price 		  = $_POST['price'];
            $status		  = $_POST['status'];
            $count		  = $_POST['count'];
            $granty_name  = $_POST['granty_name'];
            $granty_time  = $_POST['granty_time'];
            $workable     = $_POST['workable'];
            $category_id  = $_POST['category_id'];
            $result_json  = array();
            if (isset($_FILES['file']['name'])) {
                $file_name = $username.'_'.time().'_'.$_FILES["file"]['name'];
                $config['file_name'] = $file_name;
                if (0 < $_FILES['file']['error']) {
                    $result_json['code'] = '1' ;//Can't upload file ;
                    $result_json['error'] = $_FILES['file']['error'];
                    echo json_encode($result_json);
                } else {
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $result_json['code']  = '5';
                        $result_json['error'] = $this->upload->display_errors();
                        echo json_encode($result_json);
                    } else {
                        $data  =  array(
                            'product_name' => $product_name,
                            'price' 			 => $price,
                            'status' 			 => $status,
                            'count' 			 => $count,
                            'details' 	   => $file_name,
                            'user_id'  		 => $user_id,
                            'insert_date'  => $insert_date,
                            'granty_time'  => $granty_time,
                            'granty_name'  => $granty_name,
                            'workable'		 => $workable,
                            'category_id'	 => $category_id
                        );
                        $res = $model_profile->insert_product($data);
                        if($res>0){
                            $result_json['code'] = '2' ;//upload and written in database successfuly ;
                            echo json_encode($result_json);
                        }else{
                            $result_json['code'] = '3' ;//Can't written in database successfuly ;
                            echo json_encode($result_json);
                        }
                    }
                }
            } else {
                $data  =  array(
                    'product_name' => $product_name,
                    'price' 			 => $price,
                    'status' 			 => $status,
                    'count' 			 => $count,
                    'details' 	   => '',
                    'user_id'  		 => $user_id,
                    'insert_date'  => $insert_date,
                    'granty_time'  => $granty_time,
                    'granty_name'  => $granty_name,
                    'workable'		 => $workable,
                    'category_id'		 => $category_id
                );
                $res = $model_profile->insert_product($data);
                if($res>0){
                    $result_json['code'] = '2' ;//written in database successfuly ;
                    echo json_encode($result_json);
                }else{
                    $result_json['code'] = '3' ;//Can't written in database successfuly ;
                    echo json_encode($result_json);
                }
            }
        }else{
            $result_json['code'] = '4' ;//fill rquire  ;
            echo json_encode($result_json);
        }
    }
    public function list_product(){
        $this->load->model('List_profile_model');
        $model_list = new List_profile_model();
        $this->middle = 'profile/list_product';
        $this->layout();
    }
    public function ajax_list(){
        $this->load->model('List_profile_model');
      	$model_list = new List_profile_model();
				$this->load->model('Profile_model');
        $model_profile = new  Profile_model();
				$username = $this->username_in_session();
        $user_id  = $model_profile->get_user_id($username);
        $user_id  = $user_id['0']->id;
		    $list = $model_list->get_datatables($user_id);
		    $data = array();
		    $no = $_POST['start'];
		    foreach ($list as $procduct) {
		        $no++;
		        $row = array();
		        $row[] = $no;
		        $row[] = $procduct->productname;
		                    $row[] = $procduct->title;
		        $row[] = $procduct->price.'تومان';
		                    $row[] = $procduct->counter;
		                    if($procduct->granty_time>0){
		                        $row[] = ' دارد '.$procduct->granty_time.' ماه - '.$procduct->granty_name;
		                    }else{
		                        $row[] = ' ندارد ';
		                    }
		                    if($procduct->workable>0){
		                        $row[] = ' بله '.$procduct->workable.' ماه' ;
		                    }else{
		                        $row[] = ' آکبند ';
		                    }
		                    $row[] =
												"<a class='btn btn-sm btn-primary' style='margin-right:5px;'  title='ویرایش' href='".base_url('index.php/profile/edit_product/'.$procduct->id.'')."' > ویرایش </a>".
												"<a class='btn btn-sm btn-danger'  title='حذف' style='margin-right:5px;' data-toggle='modal' data-target='#exampleModal' data-whatever='$procduct->id'>حذف </a>";
		                    $data[] = $row;
		    }

		    $output = array(
		                    "draw" => $_POST['draw'],
		                    "recordsFiltered" => $model_list->count_filtered(),
		                    "data" => $data
		            );
		    //output to json format
		    echo json_encode($output);
		}
    public function ajax_edit($id){
        $this->load->model('List_profile_model');
        $model_list = new List_profile_model();
        $data = $model_list->product_join_category_get_by_id($id);
        //$data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
    }
    public function edit_product($id){
        if($id>0){

            $this->load->model('Profile_model');
            $model_profile = new  Profile_model();
            $username = $this->username_in_session();
            $user_id  = $model_profile->get_user_id($username);
            $user_id  = $user_id['0']->id;
            $res = $model_profile->procduct_info_with_get_by_id_and_user_id($id,$user_id);
            if($res->num_rows()>0){
            foreach ($res->result() as $procduct) {
                $info['id'] = $procduct->id;
                $info['category_id'] = $procduct->category_id;
                $info['price'] = $procduct->price;
                $info['counter'] = $procduct->counter;
                $info['status'] = $procduct->status;
                $info['productname'] = $procduct->productname;
                $info['details'] = $procduct->details;
                $info['granty_time'] = $procduct->granty_time;
                $info['granty_name'] = $procduct->granty_name;
                $info['workable'] = $procduct->workable;
                $info['category_id'] = $procduct->category_id;
                $info['details'] =$procduct->details;
            }
            $result  =  $model_profile->list_category();
            $info['html'] = '';
            $count = 1;
            $padding =0;
            foreach ($result as $value) {
                    if($value->parent_id=='0'){
                            $info['html'].= "<option value=".$value->id.">"." $value->title "."</option>";
                            $info['html'].=$this->sub_category($result,$value,$count,$padding);

                    }
            }
            $this->data =  $info;
      $this->middle = 'profile/edit_product';
        $this->layout();
            }else{
                redirect(''.base_url().'index.php/login/');
            }
        }else{
            redirect(''.base_url().'index.php/login/');
        }
    }
    public function update_product(){
        $this->load->model('Profile_model');
        $model_profile = new  Profile_model();
        $date  = new jDateTime(true, true, 'Asia/Tehran');
        $insert_date = $date->date("Y-m-d H:i:s", false, false);
        $modify_date = $date->date("Y-m-d H:i:s", false, false);
        $username = $this->username_in_session();
        $user_id  = $model_profile->get_user_id($username);
        $user_id  = $user_id['0']->id;
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'pdp|txt|zip';
        $config['max_filename'] = '520';
        $config['max_size'] = '1024'; //1 MB
        if((!empty($_POST['product_name'])) && (!empty($_POST['price'])) && ((!empty($_POST['status'])) || $_POST['status'] == '0') && ((!empty($_POST['granty_time'])) || $_POST['granty_time'] == '0') &&
        ((!empty($_POST['granty_name'])) || $_POST['granty_name'] == '0') && ((!empty($_POST['category_id'])) || $_POST['category_id'] == '0') && ((!empty($_POST['workable'])) || $_POST['workable'] == '0')
        && ((!empty($_POST['count'])) || $_POST['count'] =='0')){
            $product_name = $_POST['product_name'];
            $price 		  = $_POST['price'];
            $status		  = $_POST['status'];
            $count		  = $_POST['count'];
            $granty_name  = $_POST['granty_name'];
            $granty_time  = $_POST['granty_time'];
            $workable     = $_POST['workable'];
            $category_id  = $_POST['category_id'];
            $id = $_POST['id'];
            $result_json  = array();
            if (isset($_FILES['file']['name'])) {
                $file_name = $username.'_'.time().'_'.$_FILES["file"]['name'];
                $config['file_name'] = $file_name;
                if(0 < $_FILES['file']['error']) {
                    $result_json['code']  = '1' ;//Can't upload file ;
                    $result_json['error'] = $_FILES['file']['error'];
                    echo json_encode($result_json);
                } else {
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $result_json['code']  = '5';
                        $result_json['error'] = $this->upload->display_errors();
                        echo json_encode($result_json);
                    } else {
                        $data  =  array(
                            'product_name' => $product_name,
                            'price' 			 => $price,
                            'status' 			 => $status,
                            'count' 			 => $count,
                            'details' 	   => $file_name,
                            'user_id'  		 => $user_id,
                            'modify_date'  => $modify_date,
                            'insert_date'  => $insert_date,
                            'granty_time'  => $granty_time,
                            'granty_name'  => $granty_name,
                            'workable'		 => $workable,
                            'id'		 			 => $id,
                            'category_id'	 => $category_id
                        );
												$res = $model_profile->procduct_count_with_get_by_id_and_user_id($data['id'],$data['user_id']);
												if($res){
														$res = $model_profile->insert_product_temp($data['id'],$data['user_id'],$data['insert_date']);
														if($res){
																$res = $model_profile->update_product($data);
																if($res>0){
																		$result_json['code'] = '2' ;//upload and written in database successfuly ;
																		echo json_encode($result_json);
																}else{
																		$result_json['code'] = '3' ;//Can't written in database successfuly ;
																		echo json_encode($result_json);
																}
														}else{
																$result_json['code'] = '3' ;//Can't written in database successfuly ;
																echo json_encode($result_json);
														}
												}else{
													redirect(''.base_url().'index.php/login/');
												}


                    }
                }
            } else {
                $data  =  array(
                    'product_name' => $product_name,
                    'price' 	   => $price,
                    'status'       => $status,
                    'count' 	   => $count,
                    'details' 	   =>$_POST['file'],
                    'user_id'  	   => $user_id,
                    'modify_date'  => $modify_date,
                    'insert_date'  => $insert_date,
                    'granty_time'  => $granty_time,
                    'granty_name'  => $granty_name,
                    'workable'	   => $workable,
                    'id'	   => $id,
                    'category_id'  => $category_id
                );
								$res = $model_profile->procduct_count_with_get_by_id_and_user_id($data['id'],$data['user_id']);
								if($res){
										$res = $model_profile->insert_product_temp($data['id'],$data['user_id'],$data['insert_date']);
										if($res){
												$res = $model_profile->update_product($data);
												if($res>0){
														$result_json['code'] = '2' ;//upload and written in database successfuly ;
														echo json_encode($result_json);
												}else{
														$result_json['code'] = '3' ;//Can't written in database successfuly ;
														echo json_encode($result_json);
												}
										}else{
												$result_json['code'] = '3' ;//Can't written in database successfuly ;
												echo json_encode($result_json);
										}
								}else{
									redirect(''.base_url().'index.php/login/');
								}
            }
        }else{
            $result_json['code'] = '4' ;//fill rquire  ;
            echo json_encode($result_json);
        }
    }
		public function ajax_delete($id){
				if($id>0){
					$this->load->model('Profile_model');
					$model_profile = new  Profile_model();
					$date  = new jDateTime(true, true, 'Asia/Tehran');
					$username = $this->username_in_session();
					$user_id  = $model_profile->get_user_id($username);
					$user_id  = $user_id['0']->id;
					$insert_date = $date->date("Y-m-d H:i:s", false, false);
					$modify_date = $date->date("Y-m-d H:i:s", false, false);
					$res = $model_profile->procduct_count_with_get_by_id_and_user_id($id,$user_id);
					if($res>0){
						$res = $model_profile->insert_product_temp($id,$user_id,$insert_date);
						if($res){
							$res = $model_profile->delete_product($id,$user_id,$modify_date);
							if($res>0){
									$result_json['code'] = '2' ;//upload and written in database successfuly ;
									echo json_encode($result_json);
							}else{
									$result_json['code'] = '3' ;//Can't written in database successfuly ;
									echo json_encode($result_json);
							}
						}else{
							$result_json['code'] = '3' ;//Can't written in database successfuly ;
							echo json_encode($result_json);
						}
					}else{
							//log out
					}
				}else{
						//log out
				}
		}
		public function setting() {
            $this->load->model('Profile_model');
            $model_profile = new  Profile_model();
            $username = $this->username_in_session();
            $user_id  = $model_profile->get_user_id($username);
            $user_id  = $user_id['0']->id;
            $info = $model_profile->user_info_with_user_id($user_id);
            foreach ($info as $value) {
            		$info['email'] = $value->email;
            		$info['password'] = $value->password;
            		$info['company'] = $value->company;
            		$info['address'] = $value->address;
            		$info['tell'] = $value->tell;
            }
            $this->data = $info;
      			$this->middle = 'profile/setting';
        		$this->layout();
    }
	  public function update_user_info(){
			$this->load->model('Profile_model');
			$model_profile = new  Profile_model();
			$username = $this->username_in_session();
			$user_id  = $model_profile->get_user_id($username);
			$user_id  = $user_id['0']->id;
			$this->load->model('Register_model');
	 	  $model_user = new  Register_model();
	 	  $email      =  $_POST['email'] ;
	 	  $company    =  $_POST['company'] ;
	 	  $address    =  $_POST['address'] ;
	 	  $tell       =  $_POST['cell_number'] ;
	 	  $data       = array(
	         'email'  	=> $email,
	         'company'   => $company,
	         'address'   => $address,
	         'tell'      => $tell,
					 'user_id'   => $user_id
	 	 );
	 		 $update = $model_profile->update_user_info($data);
	 		 if($update){
	 			 $status['success'] = true;
	 		 }else{
	 			 $status['fail_server'] = true;
	 		 }
	 	   echo json_encode($status);
	 	}
		public function update_user_password(){
			$this->load->model('Profile_model');
			$model_profile = new  Profile_model();
			$username = $this->username_in_session();
			$user_id  = $model_profile->get_user_id($username);
			$user_id  = $user_id['0']->id;
			$this->load->model('Register_model');
	 	  $model_user = new  Register_model();
	 	  $old_password      =  $_POST['old_password'] ;
	 	  $new_password      =  $_POST['new_password'] ;
			$res  = $model_profile->check_user_password($old_password,$user_id);
			if($res){

				$new_password  	= $new_password;
				$update = $model_profile->update_user_password($new_password,$user_id);
				if($update){
					 $status['success'] = true;
				}else{
					 $status['fail_server'] = true;
				}

			}else{
					$status['fail_server'] = true;
			}
			echo json_encode($status);
	 	}
}
