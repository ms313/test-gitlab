<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct(){
 			parent::__construct();
             $this->check_isvalidated();
 	}
    private function check_isvalidated(){
            if(! $this->session->userdata('validated_admin')){
                redirect('admin/login');
            }
    }
    private function username_in_session(){
            return $this->session->userdata('username');
    }
	public function index(){
        $this->load->model('admin/index_model');
        $index_model = new  Index_model();
        $count_products = $index_model->count_of_all_products();
        $info['count_products'] = $count_products;
        $count_users = $index_model->count_of_all_users();
        $info['count_users'] = $count_users;
        $count_categories = $index_model->count_of_all_categories();
        $info['count_categories'] = $count_categories;
        $this->data = $info;
		$this->middle ='admin/home';
		$this->admin_layout();
	}
}
