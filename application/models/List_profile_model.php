<?php defined('BASEPATH') OR exit('No direct script access allowed');

class List_profile_model extends CI_Model {
    public  $table   = 'procducts';
    public  $user_id = null;
    var $column_order = array(null, 'price','counter','productname','details','granty_time'); //set column field database for datatable orderable
    var $column_search = array('granty_name', 'price','counter','productname','details','granty_time'); //set column field database for datatable searchable
    var $order = array('procducts.id' => 'asc'); // default order

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    private function _get_datatables_query()
    {
        $user_id = $this->user_id;
        $this->db->select('procducts.* , ca.title as title');
        $this->db->from($this->table);
        $this->db->join('category as ca', ''.$this->table.'.category_id = ca.id');
        $this->db->where(array(''.$this->table.'.user_id'=>$user_id,''.$this->table.'.active'=>'1'));
        //SELECT procducts.* ,ca.title as title FROM `procducts` JOIN category as ca ON ca.id = procducts.category_id WHERE procducts.user_id=1        $i = 0;
        $i=0;
        foreach ($this->column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($user_id){
        $this->user_id = $user_id;
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function product_join_category_get_by_id($id){
  		$this->db->from($this->table);
      $this->db->join('category', ''.$this->table.'.category_id = category.id');
  		$this->db->where(''.$this->table.'.id',$id);
  		$query = $this->db->get();

  		return $query->row();
  	}


}
