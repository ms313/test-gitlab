<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
 		parent::__construct();
 	}
	public function count_user_of_product($user_id){
		$this->db->where(array('user_id'=>$user_id,'active'=>'1'));
		$this->db->from('procducts');
		$cnt = $this->db->count_all_results();
		return $cnt;
	}
	public function get_user_id($username){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('username', $username);
		$query = $this->db->get();
		return $query->result();
	}
	public function insert_product($data){
		$this->db->set('user_id', $data['user_id']);
		$this->db->set('price', $data['price']);
		$this->db->set('counter', $data['count']);
		$this->db->set('status', $data['status']);
		$this->db->set('productname', $data['product_name']);
		$this->db->set('details', $data['details']);
		$this->db->set('insert_date',$data['insert_date']);
		$this->db->set('granty_time',$data['granty_time']);
		$this->db->set('granty_name',$data['granty_name']);
		$this->db->set('workable',$data['workable']);
		$this->db->set('category_id',$data['category_id']);
		$this->db->insert('procducts');
		return $this->db->insert_id();
	}
	public	function list_category(){
		$this->db->from('category');
		//$this->db->order_by("parent_id", " DESC");
		$query  =  $this->db->get();
		return $query->result();
	}
	public function procduct_info_with_get_by_id_and_user_id($id,$user_id){
		$this->db->select('*');
		$this->db->from('procducts');
		$this->db->where(array('id'=>$id,'user_id'=>$user_id));
		$query = $this->db->get();
		return $query;
	}
  public function update_product($data){


        $update = array(
            'category_id' => $data['category_id'],
            'price' => $data['price'],
            'status' => $data['status'],
            'productname' => $data['product_name'],
            'details' => $data['details'],
            'granty_time' => $data['granty_time'],
            'granty_name' => $data['granty_name'],
            'workable' => $data['workable'],
            'counter' => $data['count'],
            'modfiy_date' => $data['modify_date']
        );
        $this->db->where(array('id' => $data['id'],'user_id'=>$data['user_id']));
        $this->db->update('procducts', $update);
        return $this->db->affected_rows();
    }
  public function insert_product_temp($id,$user_id,$insert_date){
        $res = $this->procduct_info_with_get_by_id_and_user_id($id,$user_id);
        if($res->num_rows()>0){
            foreach ($res->result() as $procduct) {
                $info['id'] = $procduct->id;
                $info['category_id'] = $procduct->category_id;
                $info['price'] = $procduct->price;
                $info['counter'] = $procduct->counter;
                $info['status'] = $procduct->status;
                $info['productname'] = $procduct->productname;
                $info['details'] = $procduct->details;
                $info['granty_time'] = $procduct->granty_time;
                $info['granty_name'] = $procduct->granty_name;
                $info['workable'] = $procduct->workable;
                $info['category_id'] = $procduct->category_id;
                $info['details'] =$procduct->details;
            }
            $this->db->set('user_id', $user_id);
            $this->db->set('insert_date',$insert_date);
            $this->db->set('products_id', $info['id']);
            $this->db->set('price', $info['price']);
            $this->db->set('counter', $info['counter']);
            $this->db->set('status', $info['status']);
            $this->db->set('productname', $info['productname']);
            $this->db->set('details', $info['details']);
            $this->db->set('granty_time',$info['granty_time']);
            $this->db->set('granty_name',$info['granty_name']);
            $this->db->set('workable',$info['workable']);
            $this->db->set('category_id',$info['category_id']);
            $this->db->insert('products_temp');
            return $this->db->insert_id();
        }else{
            return false;
        }

	}
	public function procduct_count_with_get_by_id_and_user_id($id,$user_id){
		$this->db->select('*');
		$this->db->from('procducts');
		$this->db->where(array('id'=>$id,'user_id'=>$user_id));
		$query = $this->db->get();
		$query = $query->num_rows();
		return $query;
	}
	public function delete_product($id,$user_id,$modify_date){
		$update = array(
				'active' => '0',
				'modfiy_date' => $modify_date
		);
		$this->db->where(array('id' => $id,'user_id'=>$user_id));
		$this->db->update('procducts', $update);
		return $this->db->affected_rows();
	}
	public function user_info_with_user_id($user_id){
			$this->db->from('users');
			$this->db->where('id',$user_id);
			$query = $this->db->get();
			return $query->result();
	}
  public function update_user_info($data){
		$update = array(
				'email' => $data['email'],
				'company' => $data['company'],
				'address' => $data['address'],
				'tell' => $data['tell'],
		);
		$this->db->where('id' , $data['user_id']);
		$result= $this->db->update('users', $update);
		return $result;
	}
	public function update_user_password($new_password,$user_id){
		$update = array(
				'password' => $new_password
		);
		$this->db->where(array('id' => $user_id));
		$result= $this->db->update('users', $update);
		return $result;
	}
	public function check_user_password($old_password,$user_id){
		$this->db->from('users');
		$this->db->where(array('password'=>$old_password,'id'=>$user_id));
		$query = $this->db->get();
		return $query->num_rows();
	}
}
