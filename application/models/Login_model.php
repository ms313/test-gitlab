<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
 		parent::__construct();
 	}
	public function check_username_password($data){
		  $username = $this->security->xss_clean($data['username']);
		  $password = $this->security->xss_clean($data['password']);
			$query  = $this->db->get_where('users',array('username'=>$username,'password'=>$password,'active'=>'1'));

			if($query->num_rows()>0){
				$data = array(
								'username' => $username,
								'validated' => true
				);

				$this->session->set_userdata($data);
				$this->session->userdata('username');
				return TRUE;
			}else{
					return FALSE;
			}
	}
}
