<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
 		parent::__construct();
 		$this->id =  -1 ;
 	}
	public function insert_user($data){
		$this->db->set('username', $data['username']);
		$this->db->set('password', $data['password']);
		$this->db->set('email', $data['email']);
		$this->db->set('tell', $data['tell']);
		$this->db->set('address', $data['address']);
		$this->db->set('company', $data['company']);
		$this->db->insert('users');
		return $this->db->insert_id();
	}
	public function check_username($data){
		$this->db->where('username', $data['username']);
		$this->db->from('users');
		$cnt = $this->db->count_all_results();
		return $cnt;
	}
}
