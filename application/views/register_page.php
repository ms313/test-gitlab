<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>
      ثبت نام
  </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/iranian-sans" type="text/css"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    body {
        line-height: 1.8;
        color: #f5f6f7;
    }
    p {font-size: 20px;}
    .margin {margin-bottom: 45px;}
    .bg-1 {
        background-color: #1abc9c; /* Green */
        color: #ffffff;
    }
    .bg-2 {
        background-color: #474e5d; /* Dark Blue */
        color: #ffffff;
    }
    .bg-3 {
        background-color: #ffffff; /* White */
        color: #555555;
    }
    .bg-4 {
        background-color: #2f2f2f; /* Black Gray */
        color: #fff;
    }
    .container-fluid {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .navbar {
        padding-top: 15px;
        padding-bottom: 15px;
        border: 0;
        border-radius: 0;
        margin-bottom: 0;
        font-size: 18px;
        font-family: 'IranianSansRegular';
    }
    .navbar-nav  li a:hover {
        color: #1abc9c !important;
    }
    .font-sans{
         font-family: 'IranianSansRegular';
         font-weight: normal;
         font-style: normal;
    }
    label{
      color:#777;
      direction: rtl;
      float: right;
      font:14px IranianSansRegular ;
    }
    .form-horizontal .control-label{
      padding-right:10%;
      float: right;
    }
    .form-horizontal .form-control{
      float: right;
      width: 35%;
    }
  </style>
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="http://www.shabakekala.com/wp-content/uploads/2016/05/logo-77.png"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">ثبت نام</a></li>
        <li><a href="<?php echo base_url();?>index.php/login/">ورود</a></li>
        <li><a href="<?php echo base_url();?>index.php/index/">
          صفحه اصلی
        </a></li>
      </ul>
    </div>
  </div>
</nav>

<!-- First Container -->
<div class="container-fluid bg-1 text-center">
  <h2 class="margin font-sans">
    <span style="color:#fff;text-align:center;" >فروشگاه آنلاین</span>
    <span style="color:#ff0000;text-align:center;">تجهیزات شبکه</span>
  </h2>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>

<!-- Second Container -->
<div class="container-fluid text-center">

  <p style="font-size:25px;direction:rtl;color:#ff0000"class="font-sans text-center">
          ثبت نام
  </p>
  <form class="form-horizontal text-center text-center">
<div class="form-group">
  <label for="username" class="col-sm-2 control-label">
    نام کاربری :
  </label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="username" name="username" placeholder="username">
  </div>
</div>
<div class="form-group">
  <label for="password" class="col-sm-2 control-label">
    گذرواژه :
  </label>
  <div class="col-sm-10">
    <input type="password" class="form-control" id="password" name="password"  placeholder="password">
  </div>
</div>
<div class="form-group">
  <label for="company" class="col-sm-2 control-label">
    نام شرکت :
  </label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="company" name="company" placeholder="company">
  </div>
</div>
<div class="form-group">
  <label for="email" class="col-sm-2 control-label">
    ایمیل :
  </label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="" name="email"  placeholder="email">
  </div>
</div>
<div class="form-group">
  <label for="address" class="col-sm-2 control-label">
    آدرس :
  </label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="address" name="address"  placeholder="address">
  </div>
</div>
<div class="form-group">
  <label for="cell_number" class="col-sm-2 control-label">
    شماره تلفن :
  </label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="cell_number" name="cell_number" placeholder="cell number">
  </div>
</div>
    <a href="#" style="width:250px;margin-bottom: 15px;" id="submit" class="btn btn-default btn-lg text-center font-sans">
  ارسال  </a>
    <a href="#"  style="width:250px;margin-bottom: 15px;" class="btn btn-default btn-lg text-center font-sans">
  بازگشت  </a>
  </form>
  <div id="info">

  </div>

</div>

<script>
$(document).ready(function() {
    $('#submit').click(function(){

        // there are many ways to get this data using jQuery (you can use the class or id also)
       var formData = {
           'username'              : $('input[name=username]').val(),
           'email'             : $('input[name=email]').val(),
           'password'    : $('input[name=password]').val(),
           'cell_number'    : $('input[name=cell_number]').val(),
           'address'    : $('input[name=address]').val(),
           'company'    : $('input[name=company]').val()
       };
       console.log(formData);
       $.ajax({
           type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
           url         : '<?php echo base_url();?>index.php/Register/reg/', // the url where we want to POST
           data        : formData, // our data object
           dataType    : 'json', // what type of data do we expect back from the server
           encode      : true
       })
       .done(function(status) {
               if(status['fail_server']){
                 var html = "<div style='float:right;direction:rtl;' class='alert alert-danger col-xs-3 text-center'><strong> خطا </strong>لطفا با مدیریت تماس بگیرید .</div>";
                 $('#info').html(html);
               }
               if(status['fail_username']){
                 var html = "<div style='float:right;direction:rtl;' class='alert alert-warning col-xs-3 text-center'><strong> خطا </strong> نام کاربری انتخاب شده از قبل وجود دارد .</div>";
                 $('#info').html(html);
               }
               if(status['success']){
                 var html = "<div style='float:right;direction:rtl;' class='alert alert-success col-xs-3 text-center'><strong>تکمیل <strong>  ثبت نام شما با موفقیت انجام شد . </div>";
                 $('#info').html(html);
               }
        });
    });
});
</script>
<!-- Footer -->
<footer style="bottom:0px;width:100%;" class="container-fluid bg-1 text-center">
  <p>طراحی شده توسط <a href="http://www.shabakekala.com/">شبکه کالا</a></p>
</footer>
</body>
</html>
