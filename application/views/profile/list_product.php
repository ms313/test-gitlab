
<link href="<?php echo base_url('template/css/dataTables.bootstrap.css')?>" rel="stylesheet">
<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header font-sans">نمایش لیست محصولات</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading font-sans">
                  جدول محصولات
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover list_product_table" id="dataTables-example_paginate">
                        <thead>
                            <tr>
                                <th>ردیف</th>
                                <th>نام کالا</th>
                                <th>دسته بندی</th>
                                <th>قیمت کالا</th>
                                <th>تعداد موجودی</th>
                                <th>وضعیت گارانتی</th>
                                <th>محصول کار کرده</th>
                                <th>عملیات/راهنما</th>
                            </tr>
                        </thead>

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /row -->
</div>
<!-- /page-wrapper -->

<script src="<?php echo base_url('template/js/jquery.dataTables.min.js')?>"></script>

<script src="<?php echo base_url('template/js/dataTables.bootstrap.min.js')?>"></script>


<script type="text/javascript">

var table;

$(document).ready(function() {

    //datatables

    table = $('#dataTables-example_paginate').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('profile/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
        "language": {
            "lengthMenu": "تعداد _MENU_ رکورد برای نمایش در هر صفحه",
            "zeroRecords": "جستجوی شما نتیجه ای در بر نداشت ",
            "info": "نمایش صفحه ای _PAGE_ از _PAGES_",
            "infoEmpty": "رکوردی برای نمایش در دسترس نیست",
            "infoFiltered": "",
            "paginate": {
                "first":      "اول",
                "last":       "آخر",
                "next":       "بعدی",
                "previous":   "قبلی"
            },
            "search":"جستجو:",
            "loadingRecords": "در حال بارگذاری...",
            "processing":     "درحال پردازش..."
        }

    });
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('#exampleModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var id = button.data('whatever') // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      modal.find('.modal-footer .delete_p').attr('onclick', 'delete_p('+id+')');
    });
});
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}
function delete_p(id){

  $.ajax({
          url : "<?php echo site_url('profile/ajax_delete')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data){
              //if success reload ajax table
              $('#exampleModal').modal('hide');
              reload_table();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error deleting data');
          }
      });
}
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
      </div>
      <div class="modal-body">
        <p class="font-sans">آیا مطمین هستید می خواهید محصول را حذف کنید ؟!</p>
        <p class="font-sans">این کار غیر قابل برگشت می باشد.</p>
      </div>
      <div class="modal-footer">
        <a class="btn btn-primary delete_p" onclick="" >بله</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">خیر</button>
      </div>
    </div>
  </div>
</div>

</div><!-- /.modal -->
<!-- End Bootstrap modal -->
