<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header font-sans">تنظیمات کاربری</h1>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading font-sans">
                  ویرایش
            </div>
            <div class="panel-body">

                <div class="col-lg-6">
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs font-sans" role="tablist">
                      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">ویرایش</a></li>
                      <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">ویرایش گذرواژه</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content" style="margin-top:15px">
                      <div role="tabpanel" class="tab-pane active" id="home">
                        <form class="form-horizontal text-center text-center font-sans">
                            <div class="form-group">
                              <label for="company" class="col-sm-2 control-label">
                                نام شرکت : <span class="require">*</span>
                              </label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="company" name="company" value="<?php if(isset($company)) echo $company; ?>">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="email" class="col-sm-2 control-label">
                                ایمیل : <span class="require">*</span>
                              </label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="email" name="email"  value="<?php if(isset($email)) echo $email; ?>">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="address" class="col-sm-2 control-label">
                                آدرس : <span class="require">*</span>
                              </label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="address" name="address"  value="<?php if(isset($address)) echo $address; ?>">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="cell_number" class="col-sm-2 control-label">
                                شماره تلفن : <span class="require">*</span>
                              </label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="cell_number" name="cell_number" value="<?php if(isset($tell)) echo $tell; ?>">
                              </div>
                            </div>
                            <a href="#" style="margin-bottom: 15px;" id="submit-genral" class="btn btn-primary btn-lg text-center font-sans">
                          ارسال  </a>
                            <a href="#"  style="margin-bottom: 15px;" class="btn btn-default btn-lg text-center font-sans">
                          بازگشت  </a>
                        </form>
                        <div id="info-genral">

                        </div>
                    </div>
                      <div role="tabpanel" class="tab-pane" id="profile">
                        <form class="form-horizontal text-center text-center font-sans">

                            <div class="form-group">
                              <label for="password" class="col-sm-2 control-label">
                                گذر واژه قدیمی : <span class="require">*</span>
                              </label>
                              <div class="col-sm-10">
                                <input type="password" class="form-control" id="password" name="old_password"  value="123456">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="password" class="col-sm-2 control-label">
                                  گذرواژه جدید : <span class="require">*</span>
                              </label>
                              <div class="col-sm-10">
                                <input type="password" class="form-control" id="new-password" name="new_password"  placeholder="password">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="password" class="col-sm-2 control-label">
                                  تکرار گذرواژه جدید : <span class="require">*</span>
                              </label>
                              <div class="col-sm-10">
                                <input type="password" class="form-control" id="new-repat-password" name="new_repat_password"  placeholder="confirm password">
                              </div>
                            </div>
                            <a href="#" style="margin-bottom: 15px;" id="submit-password" class="btn btn-primary btn-lg text-center font-sans">
                          ارسال  </a>
                            <a href="#"  style="margin-bottom: 15px;" class="btn btn-default btn-lg text-center font-sans">
                          بازگشت  </a>
                          <div id="info-password">

                          </div>
                        </form>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <script>
      $(document).ready(function() {
          $('#submit-genral').click(function(){

              // there are many ways to get this data using jQuery (you can use the class or id also)
             var formData = {
                 'email'              : $('input[name=email]').val(),
                 'cell_number'    : $('input[name=cell_number]').val(),
                 'address'    : $('input[name=address]').val(),
                 'company'    : $('input[name=company]').val()
             };
             $.ajax({
                 type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                 url         : '<?php echo base_url();?>index.php/profile/update_user_info/', // the url where we want to POST
                 data        : formData, // our data object
                 dataType    : 'json', // what type of data do we expect back from the server
                 encode      : true
             })
             .done(function(status) {
                     if(status['fail_server']){
                       var html = "<div style='float:right;direction:rtl;' class='alert alert-danger col-lg-6 text-center'><strong> خطا </strong>لطفا با مدیریت تماس بگیرید .</div>";
                       $('#info-genral').html(html);
                     }
                     if(status['success']){
                       var html = "<div style='float:right;direction:rtl;' class='alert alert-success col-lg-6 text-center'><strong>تکمیل <strong> اطلاعات شما با موفقیت بروزرسانی شد .  </div>";
                       $('#info-genral').html(html);
                     }
              });
          });
          $('#submit-password').click(function(){
              var new_repat_password = $('input[name=new_repat_password]').val();
              var new_password = $('input[name=new_password]').val();
              console.log(new_password+'ddd'+new_repat_password)
              if(new_repat_password == new_password){


                    // there are many ways to get this data using jQuery (you can use the class or id also)
                   var formData = {
                       'old_password'              : $('input[name=old_password]').val(),
                       'new_password'    : $('input[name=new_password]').val(),

                   };
                   $.ajax({
                       type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                       url         : '<?php echo base_url();?>index.php/profile/update_user_password/', // the url where we want to POST
                       data        : formData, // our data object
                       dataType    : 'json', // what type of data do we expect back from the server
                       encode      : true
                   })
                   .done(function(status) {
                           if(status['fail_server']){
                             var html = "<div style='float:right;direction:rtl;' class='alert alert-danger col-lg-6 text-center'><strong> خطا </strong>گذرواژه قدیمی اشتباه می باشد .</div>";
                             $('#info-password').html(html);
                           }
                           if(status['success']){
                             var html = "<div style='float:right;direction:rtl;' class='alert alert-success col-lg-6 text-center'><strong>تکمیل <strong> اطلاعات شما با موفقیت بروزرسانی شد .  </div>";
                             $('#info-password').html(html);
                           }
                    });
                  }else{
                    var html = "<div style='float:right;direction:rtl;' class='alert alert-danger col-lg-6 text-center'><strong> خطا </strong>پسورد های جدید باهم مطابقت ندارند  .</div>";
                    $('#info-password').html(html);
                  }

          });

      });
      </script>
