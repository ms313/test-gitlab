    <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header font-sans">ویرایش محصول </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    مشخصات کالا
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                          <form enctype="multipart/form-data" method="post" >
                                <div class="form-group">
                                    <label> نام کالا : <span class="require">*</span></label>
                                    <input class="form-control" type="text" id="procduct_name" value="<?php if(isset($productname)) echo $productname;?>">
                                    <p class="help-block">بعنوان مثال : سویچ سیسکو ۲۸۱۱</p>
                                </div>
                                <div class="form-group">
                                    <label> قیمت : <span class="require">*</span></label>
                                    <input class="form-control" type="text" id="price" value="<?php  if(isset($price)) echo $price;?>">
                                </div>
                                <div class="form-group">
                                  <label> وضعیت موجودی : <span class="require">*</span> </label>
                                        <label class="radio-inline">
                                          <input type="radio" <?php if($status==1) echo "checked" ;?> name="status" checked id="optionsRadiosInline2" value="1">موجود
                                        </label>

                                          <label class="radio-inline">
                                              <input type="radio" <?php if($status==0) echo "checked" ;?> name="status" id="optionsRadiosInline2" value="0">ناموجود
                                          </label>
                                </div>
                                <div class="form-group" id="counter" style="display:none;">
                                    <label> تعداد : <span class="require">*</span></label>
                                    <input class="form-control"  type="text" id="count" value="<?php if(isset($counter)) echo $counter; ?>">
                                </div>
                                <div class="form-group">
                                  <label> گارانتی : <span class="require">*</span> </label>
                                  <label class="radio-inline">
                                      <input  type="radio" <?php if($granty_time>0) echo "checked" ?> name="granty_flag" id="optionsRadiosInline1" value="1">دارد
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" <?php if($granty_time==0) echo "checked" ?> name="granty_flag" id="optionsRadiosInline2" value="0">ندارد
                                  </label>
                                </div>
                                <div class="form-group" id="granty" style="display:none;">
                                    <label> مدت گارانتی : <span class="require">*</span></label>
                                    <input class="form-control"  type="text" id="granty_time" value="<?php if(isset($granty_time)) echo $granty_time; ?>">
                                    <label> نام شرکت ارایه دهنده گارانتی : <span class="require">*</span></label>
                                    <input class="form-control"  type="text" id="granty_name" value="<?php if(isset($granty_name)) echo $granty_name; ?>">
                                </div>
                                <div class="form-group">
                                  <label> قبلا استفاده شده است  : <span class="require">*</span> </label>
                                  <label class="radio-inline">
                                      <input  type="radio" <?php if($workable>0) echo "checked" ?> name="workable_flag" id="optionsRadiosInline1" value="1">بله
                                  </label>
                                  <label class="radio-inline">
                                      <input type="radio" <?php if($workable==0) echo "checked" ?> name="workable_flag" id="optionsRadiosInline2" value="0">خیر
                                  </label>
                                </div>
                                <div class="form-group" id="workable" style="display:none;">
                                  <label> چند مدت  کار کرده است ؟  <span class="require">*</span></label>
                                    <input class="form-control"  type="text" id="workable_time" value="<?php if(isset($workable)) echo $workable;?>" >
                                    <p class="help-block">بعنوان مثال : ۱۰ روز یا ۱۰ ماه کارکرده است	</p>
                                </div>
                                <div class="form-group" id="category">
                                  <label> محصول مربوط به کدام دسته بندی میباشد .  <span class="require">*</span></label>
                                  <select class="form-control" title="Choose Plan" style="direction:ltr;" id="category_id">
                                        <?php echo $html;?>
                                  </select>
                                </div>
                                <div class="form-group">
                                    <label> جزییات محصول :
                                        <?php if(isset($details) && $details !==''){?>
                                            <span id="details">آیا می خواهید فایل راهنمایی موجود(دانلود) را تغییر دهید؟</span>
                                            <label class="radio-inline">
                                                <input type="radio" name="upload_flag" id="optionsRadiosInline1" value="1">بله
                                            </label>
                                            <label class="radio-inline">
                                                <input checked="checked" type="radio" name="upload_flag" id="optionsRadiosInline2" value="0">خیر
                                            </label>
                                        <?php }else{ ?>
                                            <span id="details">محصول مورد نظر شما هیچ فایل راهنمایی ندارد آیا می خواهید فایل راهنما اضافه کنید ؟!</span>
                                            <label class="radio-inline">
                                                <input type="radio" name="upload_flag" checked id="optionsRadiosInline1" value="1">بله
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="upload_flag" id="optionsRadiosInline2" value="0">خیر
                                            </label>
                                        <?php }?>
                                    </label>
                                    <div style="display:none;" id="upload_file">
                                            <input type="file" name="userfile" id="file">
                                    </div>
                                </div>
                                <button class="btn btn-default" id="upload" type="button">ارسال</button>
                                <button class="btn btn-default" type="button">بازگشت</button>
                            </form>
                            <div class="col-lg-12" style="margin-top:25px;" id="msg"></div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    </div>
    <script>
      $(document).ready(function(){
        var status = '' ;
        var count = '' ;
        var granty = '' ;
        var granty_name = '' ;
        var granty_time = '' ;
        var workable = '';
        var workable_time = '';
        var category_id = 0 ;
        var granty_flag = 0;
        var id  = "<?php echo $id;?>";
        var category  ="<?php echo $category_id ;?>";
        var upload_flag = '';
        var file_data  = "<?php echo $details ; ?>";
        $('select option[value="'+category+'"]').attr("selected",true);
        status = $('input[name=status]:checked').val();
        upload_flag = $('input[name=upload_flag]:checked').val();
        granty_flag = $('input[name=granty_flag]:checked').val();
        workable = $('input[name=workable_flag]:checked').val();
        if(upload_flag==1){
            $('#upload_file').css('display','inline');
        }else{
            $('#upload_file').css('display','none');
        }
        if(granty_flag==1){
          $('#granty').css('display','inline');

        }else{
          $('#granty').css('display','none');
              $('#granty_time').val('0');
              $('#granty_name').val('0');
              granty = 0 ;
        }

        /////////////////
        if(status==1){
          $('#counter').css('display','inline');

        }else{
          $('#counter').css('display','none');
          $('#count').val('0');
          status = 0 ;
        }

        ///////
        if(workable>0){
          $('#workable').css('display','inline');

        }else{
          $('#workable').css('display','none');
          $('#workable').val('0');
          workable = 0 ;
        }
        ///////////
        $('input[name=status]').on('change', function() {
          status = $('input[name=status]:checked').val();
          if(status==1){
              $('#counter').css('display','inline');

          }else{
              $('#counter').css('display','none');
              $('#count').val('0');
              status = 0 ;
          }
          console.log(status+'=>'+count);
        });
        /////////////////////
        $('input[name=upload_flag]').on('change', function() {
          upload_flag = $('input[name=upload_flag]:checked').val();
          if(upload_flag==1){
                $('#upload_file').css('display','inline');
          }else{
                $('#upload_file').css('display','none');
          }
        });
        ////////////////////
        $('input[name=workable_flag]').on('change', function() {
          workable = $('input[name=workable_flag]:checked').val();
          if(workable==1){
              $('#workable').css('display','inline');

          }else{
              $('#workable').css('display','none');
              $('#workable_time').val('0');
              workable_time = 0 ;
          }
          console.log(status+'=>'+count);
        });
        $('input[name=granty_flag]').on('change', function() {
          granty = $('input[name=granty_flag]:checked').val();
          if(granty==1){
              $('#granty').css('display','inline');
          }else{
              $('#granty').css('display','none');
              $('#granty_time').val('0');
              $('#granty_name').val('0');
              granty = 0 ;
          }
          console.log(status+'=>'+count);
        });
        $('#upload').on('click', function () {
               var form_data = new FormData();
               //var file_data = '';
               var product_name  = $('#procduct_name').val();
               var price  = $('#price').val();
               count      = $('#count').val();
               granty_name      = $('#granty_name').val();
               granty_time      = $('#granty_time').val();
               workable_time         = $('#workable_time').val();

               if(upload_flag=='1'){
                   file_data = $('#file').prop('files')[0];
                   if(typeof file_data == 'undefined'){
                      file_data = '';
                   }else{
                     file_data = file_data;
                   }
               }else{
                 file_data = file_data;
               }
               console.log(upload_flag);
               //console.log(file_data);
               category_id = $('#category_id').val();
               form_data.append('file', file_data);
               form_data.append('product_name', product_name);
               form_data.append('price', price);
               form_data.append('status', status);
               form_data.append('count', count);
               form_data.append('granty_time', granty_time);
               form_data.append('granty_name', granty_name);
               form_data.append('workable', workable_time);
               form_data.append('category_id', category_id);
               form_data.append('id', id);
               $.ajax({
                   url: '<?php echo base_url()?>/index.php/Profile/update_product/', // point to server-side controller method
                   dataType: 'text', // what to expect back from the server
                   cache: false,
                   contentType: false,
                   processData: false,
                   data: form_data,
                   type: 'post',
                   dataType    : 'json', // what type of data do we expect back from the server
                   success: function (response) {
                       if(response['code'] == '1'){
                         var html = "<div style='float:right;direction:rtl;' class='alert alert-danger col-xs-6'><strong> خطا </strong> حجم فایل خود را چک نمایید. </div>";
                         $('#msg').html(html); // display success response from the server

                       }
                       if(response['code'] == '2'){
                         var html = "<div style='float:right;direction:rtl;' class='alert alert-success col-xs-6'><strong>تکمیل <strong> مشخصات کالای شما با موفقیت ثبت شد ! </div>";
                         $('#msg').html(html); // display success response from the server
                         window.location.href="<?php echo base_url();?>index.php/profile/";

                       }
                       if(response['code'] == '3'){
                         var html = "<div style='float:right;direction:rtl;' class='alert alert-danger col-xs-6'><strong> خطا </strong> متاسفانه مشکلی پیش آمده با مدیریت تماس بگیرید. </div>";
                         $('#msg').html(html); // display success response from the server

                       }
                       if(response['code'] == '4'){
                         var html = "<div style='float:right;direction:rtl;' class='alert alert-danger col-xs-6 '><strong> خطا </strong> موارد ستاره دار پر کنید !  </div>";
                         $('#msg').html(html); // display success response from the server
                       }
                       if(response['code'] == '5'){
                         var html = "<div style='float:right;direction:rtl;' class='alert alert-danger col-xs-6'><strong> خطا </strong>"+response['error']+"</div>";
                         $('#msg').html(html); // display success response from the server
                       }
                   },
                   error: function (response) {
                       $('#msg').html(response); // display error response from the server
                   }
               });
         });
      });
    </script>
