<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>پنل فروشندگان شبکه کالا</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>template/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/iranian-sans" type="text/css"/>
  <script src="<?php echo base_url();?>template/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>template/js/bootstrap.min.js"></script>
  <style>
  body {
      line-height: 1.8;
      color: #f5f6f7;
  }
  p {font-size: 20px;}
  .margin {margin-bottom: 45px;}
  .bg-1 {
      background-color: #1abc9c; /* Green */
      color: #ffffff;
  }
  .bg-2 {
      background-color: #474e5d; /* Dark Blue */
      color: #ffffff;
  }
  .bg-3 {
      background-color: #ffffff; /* White */
      color: #555555;
  }
  .bg-4 {
      background-color: #2f2f2f; /* Black Gray */
      color: #fff;
  }
  .container-fluid {
      padding-top: 10px;
      padding-bottom: 10px;
  }
  .navbar {
      padding-top: 15px;
      padding-bottom: 15px;
      border: 0;
      border-radius: 0;
      margin-bottom: 0;
      font-size: 18px;
      font-family: 'IranianSansRegular';
  }
  .navbar-nav  li a:hover {
      color: #1abc9c !important;
  }
  .font-sans{
       font-family: 'IranianSansRegular';
       font-weight: normal;
       font-style: normal;
  }
  </style>
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="http://www.shabakekala.com/wp-content/uploads/2016/05/logo-77.png"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url();?>index.php/register/">ثبت نام</a></li>
        <li><a href="<?php echo base_url();?>index.php/login/">ورود</a></li>
        <li><a href="#">سوالات متداول</a></li>
      </ul>
    </div>
  </div>
</nav>

<!-- First Container -->
<div class="container-fluid bg-1 text-center">
  <h2 class="margin font-sans">
    <span style="color:#fff;text-align:center;" >فروشگاه آنلاین</span>
    <span style="color:#ff0000;text-align:center;">تجهیزات شبکه</span>
  </h2>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>

<!-- Second Container -->
<div class="container-fluid bg-2 text-center">
  <p style="font-size:25px;direction:rtl;" class="font-sans text-center">برای وارکدن محصولات خود ابتدا باید وارد/ثبت نام کنید.</p>
  <a  style="width:250px;margin-bottom: 15px;" class="btn btn-default btn-lg text-center font-sans" href="<?php echo base_url();?>index.php/register/">
     ثبت نام
  </a>
  <a href="<?php echo base_url();?>index.php/login/"  style="width:250px;margin-bottom: 15px;" class="btn btn-default btn-lg text-center font-sans">
    ورود
  </a>
</div>

<!-- Third Container (Grid) -->
<div class="container-fluid bg-3 text-center">
  <h3 class="margin">Where To Find Me?</h3><br>
  <div class="row">
    <div class="col-sm-4">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <img src="birds1.jpg" class="img-responsive margin" style="width:100%" alt="Image">
    </div>
    <div class="col-sm-4">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <img src="birds2.jpg" class="img-responsive margin" style="width:100%" alt="Image">
    </div>
    <div class="col-sm-4">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <img src="birds3.jpg" class="img-responsive margin" style="width:100%" alt="Image">
    </div>
  </div>
</div>

<!-- Footer -->
<footer style="bottom:0px;position:absolute;width:100%;" class="container-fluid bg-1 text-center">
  <p>طراحی شده توسط <a href="http://www.shabakekala.com/">شبکه کالا</a></p>
</footer>

</body>
</html>
