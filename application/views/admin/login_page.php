<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>صفحه مدیریت</title>
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/iranian-sans" type="text/css"/>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>template/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url()?>template/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo base_url()?>template/css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>template/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url()?>template/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url()?>template/css/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title font-sans">شما جهت ورود به پنل باید لاگین کنید.</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                            <fieldset>
                                <div class="form-group" style="direction:ltr;">
                                    <input class="form-control" placeholder="inter you'r username or email" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group" style="direction:ltr;">
                                    <input class="form-control" placeholder="enter you'r password" name="password" name="password" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <a id="submit" class="btn btn-lg btn-success btn-block font-sans">وارد شوید</a>
                            </fieldset>
                        </form>
                        <div id="info"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url()?>template/js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>template/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>template/js/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>template/js/sb-admin-2.js"></script>
    <script>
            $(document).ready(function(){
                 $('#submit').click(function(){
                   var formData = {
                       'username'              : $('input[name=username]').val(),
                       'password'    : $('input[name=password]').val(),
                   };
                   console.log(formData);
                   $.ajax({
                       type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                       url         : '<?php echo base_url();?>index.php/admin/Login/user/', // the url where we want to POST
                       data        : formData, // our data object
                       dataType    : 'json', // what type of data do we expect back from the server
                       encode      : true
                   }).done(function(status) {
                       if(status['fail_username']){
                         var html = "<div style='float:right;direction:rtl;margin-top:15px;' class='alert alert-danger col-xs-12  font-sans text-center'><strong> خطا </strong> نام کاربری یا گذرواژه شما اشتباه می باشد با مدیریت تماس بگیرید . </div>";
                         $('#info').html(html);
                       }
                       if(status['success']){
                         var html = "<div style='float:right;direction:rtl;margin-top:15px;' class='alert alert-success col-xs-12 font-sans text-center'><strong> تکمیل  <strong> تا چند لحظه ای دیگر به پنل مدیریت خود انتقال داده می شوید . </div>";
                         $('#info').html(html);
                         window.location.href = "<?php echo base_url();?>index.php/admin/index/";
                       }
                   });
                });
            });
    </script>
</body>

</html>